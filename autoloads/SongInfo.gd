extends Node

signal beat_changed
signal sync_requested

var bpm: int
var seconds_per_beat: float

func set_song_infos(new_bpm: int) -> void:
    self.bpm = new_bpm
    seconds_per_beat = 60.0 / bpm