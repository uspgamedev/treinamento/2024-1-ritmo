class_name Boss
extends CharacterBody2D

signal boss_killed

@onready var player: Player = get_tree().get_first_node_in_group("player")
@onready var jump_hit_box: Area2D = $JumpHitBox
@onready var roll_hit_box: Area2D = $RollHitBox
@onready var jump_indicator: MeshInstance2D = $JumpIndicator
@onready var animated_sprite: AnimatedSprite2D = $AnimatedSprite2D

@export var life: int = 30

var movements = [jump_smash, jump_smash, roll]
var movement_index: int = 0

var tween: Tween

var dead: bool = false

var roll_speed: int = 300
var stunned: bool = false

@export var bossdthevent: EventAsset # FMOD
var bossdthinstance: EventInstance
@export var bossdmgevent: EventAsset # FMOD
var bossdmginstance: EventInstance
@export var bossrollevent: EventAsset # FMOD
var bossrollinstance: EventInstance
@export var bosscrashevent: EventAsset # FMOD
var bosscrashinstance: EventInstance
@export var bossjumpevent: EventAsset # FMOD
var bossjumpinstance: EventInstance

func jump_smash(target: Vector2=player.global_position + Vector2(0, -64)) -> void:
	if animated_sprite.animation != "land":
		animated_sprite.play("jumping_first")
		bossjumpinstance = FMODRuntime.create_instance(bossjumpevent) # FMOD
		bossjumpinstance.start()
	else:
		animated_sprite.play("jumping_second")
		bossjumpinstance = FMODRuntime.create_instance(bossjumpevent) # FMOD
		bossjumpinstance.start()

	var direction: Vector2 = global_position.direction_to(target)
	var distance: float = global_position.distance_to(target)
	var start_position: Vector2 = global_position

	tween = get_tree().create_tween()

	tween.tween_property(self, "global_position", (start_position + direction * distance) + Vector2(0, -800), 0.8).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN)

	jump_indicator.global_position = target + Vector2(0, 43.2)
	jump_indicator.show()

	tween.tween_callback(animated_sprite.play.bind("falling"))
	tween.tween_property(self, "global_position", target, 0.5).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN).set_delay(1)

	await tween.finished

	animated_sprite.play("land")
	bosscrashinstance = FMODRuntime.create_instance(bosscrashevent) # FMOD
	bosscrashinstance.start()
	jump_hit_box.get_node("CollisionShape2D").disabled = false
	await get_tree().create_timer(.1).timeout
	jump_hit_box.get_node("CollisionShape2D").disabled = true
	jump_indicator.hide()
	$CollisionShape2D.set_deferred("disabled", false)

func roll() -> void:
	var side: int = randi_range(0, 1)
	jump_smash(Vector2((1 - side) * 1136 + (70 if side == 1 else - 70), player.global_position.y - 32))
	await tween.finished
	await get_tree().create_timer(1.).timeout
	animated_sprite.flip_h = side == 0
	animated_sprite.play("roll")
	bossrollinstance = FMODRuntime.create_instance(bossrollevent) # FMOD
	bossrollinstance.start()
	roll_hit_box.get_node("CollisionShape2D").disabled = false
	tween = get_tree().create_tween()
	tween.tween_property(self, "global_position", Vector2(side * 1136 + ( - 70 if side == 1 else 70), global_position.y), 1)

	await tween.finished
	roll_hit_box.get_node("CollisionShape2D").disabled = true
	animated_sprite.play("roll_crash")
	bosscrashinstance = FMODRuntime.create_instance(bosscrashevent) # FMOD
	bosscrashinstance.start()

	await animated_sprite.animation_finished
	animated_sprite.play("stun")
	stunned = true

	await get_tree().create_timer(2.).timeout
	stunned = false

func lose_life() -> void:
	if dead:
		return
	life -= 1
	damage_sound()
	$AnimationPlayer.stop()
	$AnimationPlayer.play("damage")
	if life <= 0:
		$CollisionShape2D.set_deferred("disabled", true)
		dead = true
		bossdthinstance = FMODRuntime.create_instance(bossdthevent) # FMOD
		bossdthinstance.start()
		$AnimationPlayer.stop()
		$AnimationPlayer.play("die")

func damage_sound():
	bossdmginstance = FMODRuntime.create_instance(bossdmgevent) # FMOD
	bossdmginstance.start()

func _on_timer_timeout():
	if stunned or dead:
		return
	if tween and tween.is_running():
		return
	$CollisionShape2D.set_deferred("disabled", true)
	movements[movement_index].call()
	movement_index = (movement_index + 1) % movements.size()

	if life <= 10:
		$Timer.wait_time = 2

func _on_hit_box_area_entered(area: Area2D):
	if area.get_parent() is Player:
		player.damaged.emit()
