class_name Enemy
extends CharacterBody2D

@export var max_speed: int = 100
@export var life: int = 6
@export var flinched: bool = false

@export var speed: int = 100
var direction: Vector2 = Vector2.ZERO

var player_in_range: bool = false
var attacking: bool = false
var knockbacked: bool = false
var times_hit: int = 0

var dead: bool = false

var left_point: Vector2
var right_point: Vector2

@onready var attack_hit_box: CollisionShape2D = $AttackHitBox/CollisionShape2D
@onready var proximity_hit_box: CollisionShape2D = $ProximityHitBox/CollisionShape2D
@onready var player: Player = get_tree().get_first_node_in_group("player")
@onready var animation_player: AnimationPlayer = $AnimationPlayer
@onready var animated_sprite: AnimatedSprite2D = $AnimatedSprite2D

@export var baquetadthevent: EventAsset # FMOD
var baquetadthinstance: EventInstance
@export var baquetadmgevent: EventAsset # FMOD
var baquetadmginstance: EventInstance

func _ready():
	set_to_max_speed()
	SongInfo.beat_changed.connect(_on_beat_changed)

func _process(_delta):
	right_point = player.get_node("RightPoint").global_position
	left_point = player.get_node("LeftPoint").global_position

	if velocity.x < - 10 and not knockbacked:
		flip_h_nodes(true)
		animated_sprite.flip_h = false
	elif velocity.x > 10 and not knockbacked:
		flip_h_nodes(false)
		animated_sprite.flip_h = true

	if ((player_in_range and attacking) or flinched) and not knockbacked:
		speed = 0

func _physics_process(_delta):
	var target_point = right_point if position.distance_to(right_point) < position.distance_to(left_point) else left_point
	direction = global_position.direction_to(target_point)
	if global_position.distance_to(target_point) > 10:
		velocity = direction * speed
	else:
		velocity = Vector2.ZERO
	move_and_slide()

func flip_h_nodes(flip_h: bool) -> void:
	attack_hit_box.position.x = -abs(attack_hit_box.position.x) if flip_h else abs(attack_hit_box.position.x)
	proximity_hit_box.position.x = -abs(proximity_hit_box.position.x) if flip_h else abs(proximity_hit_box.position.x)

func attack():
	if player_in_range and not attacking and not dead:
		attacking = true
		animated_sprite.play("start_attack")
		await get_tree().create_timer(SongInfo.seconds_per_beat).timeout
		if not flinched and not dead:
			animated_sprite.play("attack")
			attack_hit_box.set_deferred("disabled", false)
			await get_tree().create_timer(.2).timeout
			attack_hit_box.set_deferred("disabled", true)
			set_to_max_speed()
		
		if not dead:
			animated_sprite.play("walk")
			attacking = false

func lose_life():
	life -= 1
	if life == 0:
		dead = true
		knockback()
		animation_player.play("die")
		baquetadmginstance = FMODRuntime.create_instance(baquetadmgevent) # FMOD
		baquetadmginstance.start()
		baquetadthinstance = FMODRuntime.create_instance(baquetadthevent) # FMOD
		baquetadthinstance.start()
		return
	if times_hit >= 2:
		knockback()
	animation_player.stop()
	animation_player.play("flinch")
	baquetadmginstance = FMODRuntime.create_instance(baquetadmgevent) # FMOD
	baquetadmginstance.start()

func knockback():
	$CollisionShape2D.set_deferred("disabled", true)
	knockbacked = true
	times_hit = 0
	speed = -3 * max_speed
	await get_tree().create_timer(.4).timeout
	speed = 0
	await get_tree().create_timer(.2).timeout
	knockbacked = false
	if not dead:
		$CollisionShape2D.set_deferred("disabled", false)
		set_to_max_speed()

func set_to_max_speed():
	speed = max_speed

func _on_beat_changed() -> void:
	attack()
	times_hit = times_hit + 1 if flinched else 0

func _on_attack_hit_box_area_entered(area: Area2D):
	if area.get_parent() is Player and area.name == "HurtBox":
		player.damaged.emit()

func _on_proximity_hit_box_area_entered(area: Area2D):
	if area.get_parent() is Player:
		player_in_range = true

func _on_proximity_hit_box_area_exited(area: Area2D):
	if area.get_parent() is Player:
		player_in_range = false
