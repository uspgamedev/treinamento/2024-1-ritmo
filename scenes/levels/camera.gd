extends Camera2D

var target: Node2D
@onready var player = get_tree().get_first_node_in_group("player")

func _process(_delta):
    if target != null:
        global_position = target.global_position
    else:
        position = Vector2.ZERO