extends Area2D

var enemies_scene: PackedScene = preload ("res://scenes/enemies/enemy.tscn")
var stage_number: int = 1
var current_stage: Node2D

func _ready():
    current_stage = $Enemies

func _process(_delta):
    if current_stage.get_child_count() == 0:
        stage_number += 1
        if has_node("Enemies"+str(stage_number)):
            current_stage = get_node("Enemies"+str(stage_number))
        else:
            call_deferred("queue_free")
            
        if current_stage != null:
            current_stage.show()
            current_stage.process_mode = Node.PROCESS_MODE_INHERIT

func _on_body_entered(body: Node2D):
    if body is Player:
        body.get_node("Camera2D").target = self
        current_stage.show()
        current_stage.process_mode = Node.PROCESS_MODE_INHERIT
        $CollisionShape2D.set_deferred("disabled", true)
        get_node("LeftWall/CollisionShape2D").set_deferred("disabled", false)
        get_node("RightWall/CollisionShape2D").set_deferred("disabled", false)