class_name Level
extends Node2D

@onready var song: Song = $Song

func _on_finish_level_body_entered(body: Node2D):
	if body is Player:
		body.call_deferred("queue_free")
		SceneManager.swap_scenes("res://scenes/levels/boss_level.tscn", get_tree().root, self)
