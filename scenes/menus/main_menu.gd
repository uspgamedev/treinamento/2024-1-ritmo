extends Control

@export var startevent: EventAsset # FMOD
var startinstance: EventInstance
@export var mmmevent: EventAsset # FMOD
var mmminstance: EventInstance

func _ready():
	mmminstance = FMODRuntime.create_instance(mmmevent)
	mmminstance.start()

func _on_button_pressed():
	SceneManager.swap_scenes("res://scenes/levels/level.tscn", get_tree().root, self)
	startinstance = FMODRuntime.create_instance(startevent)
	startinstance.start()
	mmminstance.stop(FMODStudioModule.FMOD_STUDIO_STOP_ALLOWFADEOUT)
