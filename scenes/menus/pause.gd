extends Control

func _input(event: InputEvent):
	if event.is_action_pressed("escape") and get_tree().current_scene is Level:
		get_tree().paused = not get_tree().paused
		AudioServer.set_bus_volume_db(0, -25.0 if get_tree().paused else 0.0)
		visible = not visible
