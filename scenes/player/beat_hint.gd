extends Node2D

@onready var player: Player = get_tree().get_first_node_in_group("player")

@onready var dynamic_ring: Sprite2D = $Dynamic

var buffer_timer: float = 0.0
var decrease_rate: float = 0.0
var alpha_rate: float = 0.0
var seconds_per_beat: float

func _ready():
    await get_parent().ready
    seconds_per_beat = get_parent().seconds_per_beat
    decrease_rate = .7 / seconds_per_beat
    alpha_rate = 1.5 / seconds_per_beat
    buffer_timer = seconds_per_beat

func _process(delta):
    buffer_timer -= delta
    dynamic_ring.scale -= Vector2.ONE * decrease_rate * delta
    dynamic_ring.modulate.a = min(dynamic_ring.modulate.a + alpha_rate * delta, 1)
    
    if player != null:
        global_position = player.global_position - Vector2(0, 50)

func _on_song_beat_changed():
    buffer_timer = seconds_per_beat
    dynamic_ring.scale = Vector2.ONE
    dynamic_ring.modulate.a = 0
