class_name Player
extends CharacterBody2D

signal damaged()

@export var max_speed: int = 0
@export var life: int = 5

@onready var animated_sprite: AnimatedSprite2D = $Animation
@onready var hit_box: Area2D = $HitBox
@onready var hurt_box: Area2D = $HurtBox
@onready var hit_box_collision_shape: CollisionShape2D = $HitBox/HitBoxCollisionShape
@onready var lifes_h_box: HBoxContainer = %Lifes

func _physics_process(_delta: float) -> void:
	movement()
	move_and_slide()

func movement() -> void:
	velocity = Input.get_vector("left", "right", "up", "down") * max_speed

func flip_h_nodes(flip_h: bool) -> void:
	hit_box.position.x = -abs(hit_box.position.x) if flip_h else abs(hit_box.position.x)
	#hurt_box.position.x = -abs(hurt_box.position.x) if flip_h else abs(hurt_box.position.x)
	animated_sprite.position.x = -abs(animated_sprite.position.x) if flip_h else abs(animated_sprite.position.x)

func lose_life() -> void:
	life -= 1
	lifes_h_box.get_child(life - 1).hide()

func _on_hit_box_body_entered(body: Node2D):
	if body is Enemy:
		body.lose_life()

	if body is Boss:
		body.lose_life()
