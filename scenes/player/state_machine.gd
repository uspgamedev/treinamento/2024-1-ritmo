class_name StateMachine
extends Node

enum States {
	STANDING,
	WALKING,
	ATTACKING,
	DAMAGED,
	FLINCHING,
	DYING,
}

var current_state: State

var animated_sprite: AnimatedSprite2D

var player: Player

func _ready() -> void:
	for child in get_children():
		if child is State:
			child.transitioned.connect(_on_state_transitioned)
	
	current_state = get_child(States.STANDING)
	current_state.enter()

func _process(delta: float) -> void:
	if current_state:
		current_state.update(delta)
		get_parent().get_node("Label").text = current_state.name

func _physics_process(delta: float) -> void:
	if current_state:
		current_state.physics_update(delta)

func _on_state_transitioned(state: States, new_state: States):
	if state != (current_state.get_index() as States):
		return
	
	current_state.exit()

	current_state = get_child(new_state)
	current_state.enter()
