extends State

var player: Player
var animation: AnimatedSprite2D

var previous_speed: int

@export var atkevent: EventAsset # FMOD
var atkinstance: EventInstance

func enter() -> void:

	player = get_tree().get_first_node_in_group("player")

	previous_speed = player.max_speed

	player.max_speed = 0

	if not player.damaged.is_connected(on_player_damaged):
		player.damaged.connect(on_player_damaged)

	animation = player.get_node("Animation")
	if not animation.animation_finished.is_connected(on_animation_finished):
		animation.animation_finished.connect(on_animation_finished)
	if not animation.frame_changed.is_connected(_on_frame_changed):
		animation.frame_changed.connect(_on_frame_changed)

	animation.play("attacking")
	
	atkinstance = FMODRuntime.create_instance(atkevent) # FMOD
	atkinstance.start()

func exit() -> void:
	player.hit_box_collision_shape.set_deferred("disabled", true)
	animation.frame_changed.disconnect(_on_frame_changed)
	animation.animation_finished.disconnect(on_animation_finished)

	player.max_speed = previous_speed

func on_player_damaged() -> void:
	transitioned.emit(state_type, States.DAMAGED)

func on_animation_finished() -> void:
	transitioned.emit(state_type, States.STANDING)

func _on_frame_changed() -> void:
	if animation.frame == 1:
		player.hit_box_collision_shape.disabled = false
