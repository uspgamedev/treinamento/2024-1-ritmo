extends State

var player: Player
var animation: AnimatedSprite2D

var previous_speed: int

@export var dmgevent: EventAsset # FMOD
var dmginstance: EventInstance

func enter() -> void:
	player = get_tree().get_first_node_in_group("player")
	player.lose_life()

	previous_speed = player.max_speed

	animation = player.get_node("Animation")

	if not animation.animation_finished.is_connected(on_animation_finished):
		animation.animation_finished.connect(on_animation_finished)
	animation.play("damaged")
	
	dmginstance = FMODRuntime.create_instance(dmgevent) # FMOD
	dmginstance.start()

func exit() -> void:
	animation.animation_finished.disconnect(on_animation_finished)
	player.max_speed = previous_speed

func on_animation_finished() -> void:
	if player.life > 0:
		transitioned.emit(state_type, States.STANDING)
	else:
		transitioned.emit(state_type, States.DYING)
