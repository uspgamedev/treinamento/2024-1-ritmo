extends State

var player: Player
var animation: AnimatedSprite2D

@export var dthevent: EventAsset #FMOD
var dthinstance: EventInstance

func enter() -> void:
	player = get_tree().get_first_node_in_group("player")
	player.max_speed = 0

	animation = player.get_node("Animation")

	if not animation.animation_finished.is_connected(on_animation_finished):
		animation.animation_finished.connect(on_animation_finished)
	animation.play("dying")
	
	dthinstance = FMODRuntime.create_instance(dthevent) #FMOD
	dthinstance.start()

func exit() -> void:
	animation.animation_finished.disconnect(on_animation_finished)

func on_animation_finished() -> void:
	get_tree().reload_current_scene()
