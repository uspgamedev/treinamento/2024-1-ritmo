extends State

var player: Player
var animation: AnimatedSprite2D

var previous_speed: int

@export var flinchevent: EventAsset #FMOD
var flinchinstance: EventInstance

func enter() -> void:
	player = get_tree().get_first_node_in_group("player")

	previous_speed = player.max_speed
	player.max_speed = 0

	animation = player.get_node("Animation")

	if not animation.animation_finished.is_connected(on_animation_finished):
		animation.animation_finished.connect(on_animation_finished)
	animation.play("flinching")
	
	flinchinstance = FMODRuntime.create_instance(flinchevent) #FMOD
	flinchinstance.start()

func exit() -> void:
	animation.animation_finished.disconnect(on_animation_finished)
	player.max_speed = previous_speed

func on_animation_finished() -> void:
	transitioned.emit(state_type, States.STANDING)
