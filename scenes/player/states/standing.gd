extends State

var player: Player
var animation: AnimatedSprite2D

func enter() -> void:
	player = get_tree().get_first_node_in_group("player")

	if not player.damaged.is_connected(on_player_damaged):
		player.damaged.connect(on_player_damaged)

	animation = player.get_node("Animation")
	animation.play("standing")

func update(_delta: float) -> void:
	if player.velocity.length() > 0:
		transitioned.emit(state_type, States.WALKING)

	if Input.is_action_just_pressed("attack"):
		var hit = get_tree().get_first_node_in_group("song").hit()
		if hit:
			transitioned.emit(state_type, States.ATTACKING)
		else:
			transitioned.emit(state_type, States.FLINCHING)

func on_player_damaged() -> void:
	transitioned.emit(state_type, States.DAMAGED)
