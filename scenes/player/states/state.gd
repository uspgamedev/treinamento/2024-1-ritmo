class_name State
extends Node

enum States {
	STANDING,
	WALKING,
	ATTACKING,
	DAMAGED,
	FLINCHING,
	DYING,
}

@export var state_type: States = States.STANDING

signal transitioned(current_state: States, new_state: States)

func enter() -> void:
	pass

func exit() -> void:
	pass

func update(_delta: float) -> void:
	pass

func physics_update(_delta: float) -> void:
	pass