extends State

var player: Player
var animation: AnimatedSprite2D

func enter() -> void:
	player = get_tree().get_first_node_in_group("player")
	
	if not player.damaged.is_connected(on_player_damaged):
		player.damaged.connect(on_player_damaged)

	animation = player.get_node("Animation")
	animation.play("walking")

func update(_delta: float) -> void:
	if player.velocity == Vector2.ZERO:
		transitioned.emit(state_type, States.STANDING)
	
	if Input.is_action_pressed("run"):
		animation.speed_scale = 1.5
		player.max_speed = 350
	else:
		animation.speed_scale = 1
		player.max_speed = 200

	if Input.is_action_just_pressed("attack"):
		var hit = get_tree().get_first_node_in_group("song").hit()
		if hit:
			transitioned.emit(state_type, States.ATTACKING)
		else:
			transitioned.emit(state_type, States.FLINCHING)
	
	if player.velocity.x < 0:
		animation.flip_h = true
		player.flip_h_nodes(true)
	if player.velocity.x > 0:
		animation.flip_h = false
		player.flip_h_nodes(false)

func on_player_damaged() -> void:
	transitioned.emit(state_type, States.DAMAGED)