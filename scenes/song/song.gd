class_name Song
extends AudioStreamPlayer

signal beat_changed

@export var bpm: int = 180
@export var tolerance_percentage: int = 20

var time_begin: int
var time_delay: float

var beat: int = 0
var beat_time: float = 0.0
var seconds_per_beat: float = 0.0

func _ready():
	seconds_per_beat = 60.0 / bpm

	time_begin = Time.get_ticks_usec()
	time_delay = AudioServer.get_time_to_next_mix() + AudioServer.get_output_latency()
	play()
	SongInfo.set_song_infos(bpm)

func _process(_delta) -> void:
	if not playing:
		return
	
	var time: float = (Time.get_ticks_usec() - time_begin) / 1000000.0

	time -= time_delay
	var last_beat = beat
	beat = int(time * bpm / 60.0)

	if beat != last_beat:
		beat_time = time
		beat_changed.emit()
		SongInfo.beat_changed.emit()

func hit():
	var tol = seconds_per_beat * tolerance_percentage / 100.0
	var time = (Time.get_ticks_usec() - time_begin) / 1000000.0 - time_delay
	if abs(time - beat_time) < tol or abs(time - beat_time - seconds_per_beat) < tol:
		return true
	return false
